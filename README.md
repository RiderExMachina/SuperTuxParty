# SuperTuxParty
An open-source party game that is meant to replicate the feel of games like
Mario Party while remaining free and open-source.

## Engine
SuperTuxParty is built with the [Godot Engine](https://godotengine.org/). The
latest stable version, 3.0.6, is used.

## Issues
If you have ideas for mini-games, design improvements or have found a bug then
please report that under Issues.

## License
All code is licensed under the GNU GPL V3.0, see LICENSE file for more
information. All other data such as art, sound, music etc. is usually
released under the [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/)
though it should always be specified which license it falls under.
## Reddit
We also have [subreddit] (https://www.reddit.com/r/SuperTuxParty/) for discussion about project. 
